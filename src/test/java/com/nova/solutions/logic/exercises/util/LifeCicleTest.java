package com.nova.solutions.logic.exercises.util;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LifeCicleTest {

  @BeforeAll
  public static void beforeAll() {
    System.out.println("---------------Antes de todos los test---------------");
  }

  @BeforeEach
  public void beforeEach() {
    System.out.println("--------Antes de cada test--------");
  }

  @Test
  public void test1() {
    System.out.println("Test 1...");
  }

  @Test
  public void test2() {
    System.out.println("Test 2...");
  }

  @Test
  public void test3() {
    System.out.println("Test 3...");
  }

  @AfterEach
  public void afterEach() {
    System.out.println("--------Despues de cada test--------");
  }

  @AfterAll
  public static void afterAll() {
    System.out.println("---------------Despues de todos los test---------------");
  }

}
